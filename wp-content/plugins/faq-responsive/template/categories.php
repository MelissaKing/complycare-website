<div class="col_half">
	<h2>Categories</h2>
	<ul class="categories"><?php 
	
    $ac_post_type = "wpsm_faq_r";
	
    $CatData = array(  'p' => $WPSM_FAQ_ID, 'post_type' => $ac_post_type, 'order' => 'ASC','orderby' => 'menu_order', 'posts_per_page' => -1);
    $catLoop = new WP_Query( $CatData );
	//print_r($loop);

	
	while ( $catLoop->have_posts() ) : $catLoop->the_post();
		//get the post id
		$post_id = get_the_ID();
		$Shortcode_Settings = unserialize(get_post_meta( $post_id, 'Wpsm_Faq_Shortcode_Settings', true));

		$option_names = array(
			"acc_sec_title" 	 => "yes",
			"acc_margin"   		 => "yes",
			"enable_toggle"    	 => "no",
			"enable_ac_border"   => "yes",
			);
			
			foreach($option_names as $option_name => $default_value) {
				if(isset($Shortcode_Settings[$option_name])) 
					${"" . $option_name}  = $Shortcode_Settings[$option_name];
				else
					${"" . $option_name}  = $default_value;
			}
		
		

		$TotalCount =  get_post_meta( get_the_ID(), 'wpsm_faq_count', true );
		$lower = strtolower(get_the_title( $post_id ));
		$scrubbed = preg_replace("/[^A-Za-z0-9 ]/", "", $lower);
		$anchor = str_replace(' ', '_', $scrubbed);
		if($TotalCount>0) 
		{
		?>
			<?php  if($acc_sec_title == 'yes' ) { ?>
            
			<li><a href="#" data-scrollto="#<?php echo $anchor; ?>"><?php echo get_the_title( $post_id ); ?> </a></li>
			<?php } ?>
			
			
			<?php
		}
	endwhile; ?>

	</ul>
</div>