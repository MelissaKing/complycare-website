
<div class="col_full">
<?php 
	
    $ac_post_type = "wpsm_faq_r";
	
    $AllData = array(  'p' => $WPSM_FAQ_ID, 'post_type' => $ac_post_type, 'order' => 'ASC','orderby' => 'menu_order');
    $loop = new WP_Query( $AllData );
	
	
	while ( $loop->have_posts() ) : $loop->the_post();
		//get the post id
		$post_id = get_the_ID();
		$Shortcode_Settings = unserialize(get_post_meta( $post_id, 'Wpsm_Faq_Shortcode_Settings', true));

		$option_names = array(
			"acc_sec_title" 	 => "yes",
			"op_cl_icon" 		 => "yes",
			"acc_title_icon"     => "yes",
			"acc_radius"      	 => "yes",
			"acc_margin"   		 => "yes",
			"enable_toggle"    	 => "no",
			"enable_ac_border"   => "yes",
			"acc_op_cl_align"    => "left",
			"acc_title_bg_clr"   => "#e8e8e8",
			"acc_title_icon_clr" => "#000000",
			"acc_open_cl_icon_bg_clr" => "#dd3333",
			"acc_open_cl_icon_ft_clr" => "#ffffff",
			"acc_desc_bg_clr"    => "#ffffff",
			"acc_desc_font_clr"  => "#000000",
			"title_size"         => "18",
			"des_size"     		 => "16",
			"font_family"     	 => "Open Sans",
			"expand_option"      =>1,
			"ac_styles"      =>1,
			"custom_css"      =>"",
			);
			
			foreach($option_names as $option_name => $default_value) {
				if(isset($Shortcode_Settings[$option_name])) 
					${"" . $option_name}  = $Shortcode_Settings[$option_name];
				else
					${"" . $option_name}  = $default_value;
			}
		
		$lower = strtolower(get_the_title( $post_id ));
		$scrubbed = preg_replace("/[^A-Za-z0-9 ]/", "", $lower);
		$anchor = str_replace(' ', '_', $scrubbed);
		
		$accordion_data = unserialize(get_post_meta( get_the_ID(), 'wpsm_faq_data', true));
		$TotalCount =  get_post_meta( get_the_ID(), 'wpsm_faq_count', true );
		if($TotalCount>0) 
		{
		?>
			<?php  if($acc_sec_title == 'yes' ) { ?>
            
			<h2 id="<?php echo $anchor; ?>" style="margin-bottom:20px;display:block;width:100%;" class="topmargin-lg"><?php echo get_the_title( $post_id ); ?> </h2>
			<?php } ?>
			<style>
					<?php
					require('style.php');	
					echo $custom_css;
					?>
					
			</style>
			<div class="wpsm_panel-group" id="wpsm_accordion_<?php echo $post_id; ?>" >
				
			<?php 	
			$i=1;
			
			foreach($accordion_data as $accordion_single_data)
			{
				 $faq_title = $accordion_single_data['faq_title'];
				 $faq_title_icon = $accordion_single_data['faq_title_icon'];
				 $enable_single_icon = $accordion_single_data['enable_single_icon'];
				 $faq_desc = $accordion_single_data['faq_desc'];
				 $faq_title_lower = strtolower($faq_title);
				 $faq_title_scrubbed = preg_replace("/[^A-Za-z0-9 ]/", "", $faq_title_lower);
				 $faq_title_anchor = str_replace(' ', '_', $faq_title_scrubbed);
				 $i;
				  switch($expand_option){
					    case "1":
						$j=1;
						break;
						case "2":
						 $j=$i;
						break;
						case "3":
						 $j=0;
						break;
					 }
				 
				?>
			
				  <!-- Inner panel Start -->
				  <div class="wpsm_panel wpsm_panel-default">
					<div class="wpsm_panel-heading" role="tab" >
					  <h3 class="wpsm_panel-title">
						<a class="<?php if($i!=1){ echo "collapsed"; } ?>"  data-toggle="collapse" data-parent="<?php if($enable_toggle=="no") { ?>#wpsm_accordion_<?php echo $post_id; ?> <?php } ?>" href="#ac_<?php echo $post_id; ?>_collapse<?php echo $i; ?>"  >
							<?php if($op_cl_icon == '1' ) 
							{ ?>
								<i class="icon icon-caret-<?php if($i==$j){ echo "up"; } else { echo "down"; } ?>"></i>
							<?php
							} ?>
							<?php if($op_cl_icon == '2' ) 
							{ ?>	
							<i class="icon icon-caret-<?php if($i==$j){ echo "up"; } else { echo "down"; } ?>"></i>
							<?php } ?>
                            
							<span class="ac_title_class">
								<?php if($faq_title == '' ) { echo "no title";  } else { echo $faq_title; } ?>
							</span>
						</a>
					  </h3>
					</div>
					<div id="ac_<?php echo $post_id; ?>_collapse<?php echo $i; ?>" class="wpsm_panel-collapse collapse_<?php echo $post_id; ?> collapse <?php if($i==$j){ echo "in"; } ?>"  >
					  <div class="wpsm_panel-body">
						<p class="topmargin-sm bottommargin-sm"><?php  echo do_shortcode($faq_desc); ?></p>

                        
                        <div class="col_half bottommargin contact-details topmargin">
                            <div id="contact-form-result" data-notify-type="success" data-notify-msg=""></div>
                            <div id="contact-response"></div>
                            <div class="heading-block bottommargin-sm">
                               <h4>Help improve this answer</h4>
                            </div>
                            <form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">
        
                                <div class="form-process"></div>
                                <div class="clear"></div>
                                <div class="col_full">
                                    <textarea class="required sm-form-control" id="contactform_message" name="contactform_message" rows="3" cols="30"></textarea>
                                </div>
        
                                <div class="col_full hidden">
                                    <input type="text" id="contactform_botcheck" name="contactform_botcheck" value="" class="sm-form-control" />
                                </div>
                                
                                <div class="col_full hidden">
                                    <input type="text" id="<?php echo $faq_title_anchor; ?>" name="contactform_faq" value="<?php echo $faq_title_anchor; ?>" class="sm-form-control" />
                                </div>
        
                                <div class="col_full">
                                    <button name="submit" type="submit" id="submit-button" tabindex="5" value="Submit" class="button button-small nomargin button-blue pull-right">Send Feedback</button>
                                </div>
                            </form>
							<script type="text/javascript">
        
                            $("#template-contactform").validate({
                                    submitHandler: function(form) {
                                        $('#contact-response').html();
                                        $.ajax({
                                            type: 'post',
                                            url: '/contact-submit',
                                            data: $(form).serializeArray(),
                                            success: function() {
                                                $('#contact-form-result').attr('data-notify-msg', '<div class="center topmargin-sm" data-animate="fadeInUp" data-delay="100"><h4 class="nobottommargin">Thanks for your helping improve our FAQ <i class="icon-ok-sign icon-large"></i></h4>/div>').html('');
                                                $.magnificPopup.close();
                                                SEMICOLON.widget.notifications($('#contact-form-result'));
                                            }, error: function(result) {
                                                $('#contact-response').html('<div class="style-msg errormsg"><div class="sb-msg"><i class="icon-remove"></i><strong>Something went wrong!</strong> Please try again, or call us directly on (03) 9563 0543.</div></div>');
                                            }
                                        });
                                    }
                                });
                            </script>
        
                            <!--<script type="text/javascript">
        
                                $("#template-contactform").validate({
                                    submitHandler: function(form) {
                                        $('.form-process').fadeIn();
                                        $(form).ajaxSubmit({
                                            target: '#contact-form-result',
                                            success: function() {
                                                $('.form-process').fadeOut();
                                                $('#template-contactform').find('.sm-form-control').val('');
                                                $('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
                                                SEMICOLON.widget.notifications($('#contact-form-result'));
                                            }
                                        });
                                    }
                                });
        
                            </script>-->
        
                        </div><!-- Contact Form End -->
                        
					  </div>
					</div>
				  </div>
				   <!-- Inner panel End -->
				
			<?php
			 $i++;
			}
			?>
			</div>
			<?php
		}
		else{
			echo "<h3> No Faq Found </h3>";
		}
	endwhile; ?>
	<script>
	jQuery(document).ready(function() {
		
			jQuery('.collapse_<?php echo $post_id; ?>').on('shown.bs.collapse', function(){jQuery(this).parent().find(".icon-caret-down").removeClass("icon-caret-up").addClass("icon-caret-up"); jQuery(this).parent().find(".wpsm_panel-heading").addClass("acc-a"); }).on('hidden.bs.collapse', function(){jQuery(this).parent().find(".icon-caret-up").removeClass("icon-caret-up").addClass("icon-caret-down"); jQuery(this).parent().find(".wpsm_panel-heading").removeClass("acc-a");});
		
		});
	</script>
		</div>