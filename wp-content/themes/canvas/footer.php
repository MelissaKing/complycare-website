<!-- Footer
		============================================= -->
		<footer id="footer" class="notopborder dark">
			<div class="container nopadding nomargin col_full nooverflow">

				<!-- Footer Widgets 
				============================================= -->
				<div class="footer-widgets-wrap clearfix">
					<div class="row clearfix">
						<div class="container">
							<div class="widget clearfix">
                            	<div class="row">
                                	<div class="col-md-10 bottommargin">
                                		<?php
											canvas_the_custom_logo();
										?>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-xs-3">
                                        <div class="widget clearfix">
                                            
                                            <?php
											$contactpageID = 51;
                                            $email = get_field("email", $contactpageID );
                                            $phone = get_field("phone", $contactpageID );
                                            $address = get_field("address", $contactpageID );
                                            ?>
                                            <i class="icon-email3"></i> <a href="mailto:<?php echo $email;?>"><?php echo $email;?></a><br/>
                                            <i class="icon-call"></i> <span><?php echo $phone;?></span><br/>
                                            <i class="icon-location"></i> <span><?php echo $address; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 widget_links">
                                        <ul data-easing="easeInOutExpo" data-speed="1500">
                                            <li><a href="/complycare/">Home</a></li>
                                            <li><a href="/complycare/features/">Features</a></li>
                                            <li><a href="/complycare/pricing/">Pricing</a></li>
                                            <li><a href="/complycare/blog/">Blog</a></li>
                                            <li><a href="/complycare/faq/">FAQ</a></li>
                                            <li><a href="/complycare/about/">About</a></li>
                                            <li><a href="/complycare/contact/">Contact</a></li>
                                            <li><a href="/complycare/privacy/">Privacy Policy</a></li>
                                            <li><a href="/complycare/terms/">Terms of Service</a></li>
                                        </ul>
                                    </div><div class="col-xs-3">
                                        <a class="button button-pink button-reveal button-fullwidth button-large">Sign up</a>
                                        <a class="button button-dark button-fullwidth button-large">login</a>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="clearfix" data-class-xs="" data-class-xxs="">
                                       		<h3>Connect with us</h3>
                                            <a href="https://www.facebook.com/Complycare-1468621570130311" class="social-icon si-facebook si-rounded">
                                                <i class="icon-facebook"></i>
                                                <i class="icon-facebook"></i>
                                            </a>
            
                                            <a href="https://www.twitter.com/Complycare" class="social-icon si-twitter si-rounded">
                                                <i class="icon-twitter"></i>
                                                <i class="icon-twitter"></i>
                                            </a>

                                            <a href="https://www.youtube.com/channel/UCzS4ixgM13qbwcfuh5L7YhQ" class="social-icon si-youtube si-rounded">
                                                <i class="icon-youtube"></i>
                                                <i class="icon-youtube"></i>
                                            </a>
                                        </div>
                                    </div>
    
                                    
                                    
                                 </div>
                              </div>
                       		</div>
						</div>
					</div>
            <div class="row clearfix">
                    <!-- Copyrights
			============================================= -->
				<div id="copyrights">
                    <div class="container clearfix">
    
                       <span> &copy; <?php echo date('Y'); ?> All Rights Reserved by Complycare.</span> <a href="/complycare/privacy/">Privacy policy</a> | <a href="/complycare/terms/">Terms &amp; conditions</a>
    
                    </div>
                </div><!-- #copyrights end -->
            </div>
   		</div><!-- .footer-widgets-wrap end -->
	</footer><!-- #footer end -->