<div class="section dark cta nomargin nobottomborder">
    <div class="container clearfix nobottomborder">
        <div class="column col_half nobottommargin">
            <div id="section-buy" class="heading-block nobottomborder bottommargin-sm bottommargin-sm page-section">
                <?php 
                $cta_title = get_field("cta_title", $pageID );
                $cta_text = get_field("cta_text", $pageID );
                $cta_button_text = get_field("cta_button_text", $pageID );
                $cta_button_link = get_field("cta_button_link", $pageID );
                ?>
                <h2><?php echo $cta_title;?></h2>
                <p><?php echo $cta_text;?></p>
             </div>
        </div>
        <div class="center col_half col_last">
            <a href="<?php echo $cta_button_link;?>" data-scrollto="#section-pricing" class="topmargin-sm button button-orange button-xlarge nobottommargin"><h2><?php echo $cta_button_text;?></h2></a>
        </div>
    </div>
</div>