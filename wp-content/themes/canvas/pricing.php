<?php /**
 * Template Name: Pricing
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
					
$pageID = get_the_ID();
$page_title = get_field("page_title", $pageID );
$section_title = get_field("section_title", $pageID );
$meta_description = get_field("seo_meta_description", $pageID);
$meta_title = get_field("seo_meta_title", $pageID);
$reason_title1 = get_field("reason_title_1", $pageID );
$reason_text1 = get_field("reason_text_1", $pageID );
$reason_title2 = get_field("reason_title_2", $pageID );
$reason_text2 = get_field("reason_text_2", $pageID );
$reason_title3 = get_field("reason_title_3", $pageID );
$reason_text3 = get_field("reason_text_3", $pageID );
$reason_title4 = get_field("reason_title_4", $pageID );
$reason_text4 = get_field("reason_text_4", $pageID );

?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'header.php';?>

	<!-- Document Title
	============================================= -->
	<meta name="description" content="<?php echo $meta_description;?>"/>
	<title><?php bloginfo( 'name' ); ?> | <?php echo $meta_title;?></title>


</head>

<body class="stretched pricing" data-loader-color="#E57B8D">



	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'menu.php';?>
        
        <section id="slider" class="slider-parallax hero-pricing">
            
			<div class="container clearfix">
                <div class="heading-block nobottomborder col_half">
                    
					<h1>
						<?php echo $page_title;?>
					</h1>

                </div>
            </div>
		</section>
        

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="nopadding content-wrap">
            
            <!-- intro -->
            
            
                <div class="section nopadding nomargin noborder">
                    <div class="container clearfix nobottommargin topmargin-lg">
                        <div class="row clearfix nomargin">
                            <div class="col_full">
                                <div class="heading-block center">
                                    <?php
                                        $page_subtitle = get_field("page_subtitle", $pageID );
                                        ?>
                                        <h2>
                                            <?php echo $page_subtitle;?>
                                        </h2>
                                </div>
                            </div>
                        </div>
                        <div class="tabs clearfix ui-tabs ui-widget ui-widget-content ui-corner-all" id="tab-1">
                            <ul class="tab-nav clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                                <li class="monthly ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="1" aria-controls="tabs-1" aria-labelledby="ui-id-1" aria-selected="true"><a href="#tabs-1" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">Billed monthly</a></li>
                                <li class="yearly ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-2" aria-labelledby="ui-id-2" aria-selected="false"><a href="#tabs-2" class="ui-tabs-anchor" role="presentation" tabindex="2" id="ui-id-2">Billed yearly</a></li>
                            </ul>
                        
                        
                        <div class="tab-container">
                            <!-- start tab 1-->
                            <div class="tab-content clearfix ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">
                                <div class="pricing bottommargin clearfix">
                                    <?php 
                                    $pricing_query = new WP_Query();
                                    $prices = get_page_by_title('Pricing');
                                    $args = array(
                                        'post_type' => 'page',
                                        'post_parent' => $prices->ID,
                                        'ignore_sticky_posts' => true,
                                        'fields' => 'ids',
                                    );
                                    $pricing_pages = $pricing_query->query($args);		
                                    $pricing_children = get_page_children( $prices -> ID, $pricing_pages );
                                    
                                    foreach (array_reverse($pricing_pages) as $price) {
                
                                        $plan_title = get_the_title($price);
                                        $price_month  = get_field("monthly_price", $price);
                                        $price_year =  get_field("yearly_price", $price);
                                        $price_educators = get_field("educators", $price);
                                        $button_text = get_field("button_text", $price);
                                    
                                   
                                    ?>
                                
                                    <div class="col-md-3">
                                          <div class="pricing-box pricing-<?php echo strtolower($plan_title)?>">
                                              <div class="pricing-title">
                                                  <h3><?php echo $plan_title?></h3>
                                              </div>
                                              <div class="pricing-price">
                                                  <span class="price-unit"></span><?php echo $price_month?>
                                                  
                                                  <?php 
                                                  $price_free = strpos($plan_title, "Lite");
                                                  if($price_free === false){?>
                                                  <span class="price-tenure">/mo</span>
                                                  <?php }?>
                                              </div>
                                              <div class="pricing-features">
                                                  <ul>
                                                      <li><?php echo $price_educators?> Educators</li>
                                                      <?php if($price_free === false){?>
                                                     <li>Unlimited users</li>
                                                      <li>GST included</li>
                                                        <?php }?>
                                                      
                                                  </ul>
                                              </div>
                                              <div class="pricing-action">
                                                  <a href="#" class="button button-pink button-lg"><?php echo $button_text?></a>
                                              </div>
                                          </div>
                                      </div>
                                      
                                      
                                      <?php }?>
                                  </div>
                             </div>
                             
                            
                         </div>
                            <!-- end tab 1-->
                            <!-- start tab 2-->
                            <div class="tab-content clearfix ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">
                                <div class="pricing bottommargin clearfix">
                                    <?php foreach (array_reverse($pricing_pages) as $price) {
                
                                        $plan_title = get_the_title($price);
                                        $price_year =  get_field("yearly_price", $price);
                                        $price_educators = get_field("educators", $price);
                                        $button_text = get_field("button_text", $price);
                                        $plan_free = strpos($plan_title, "Lite");
                                    if($plan_free === false){
                                    ?>
                                      
                                      <div class="col-md-4">
                                          <div class="pricing-box pricing-<?php echo strtolower($plan_title)?>">
                                              <div class="pricing-title">
                                                  <h3><?php echo $plan_title?></h3>
                                              </div>
                                              <div class="pricing-price">
                                                  <?php echo $price_year?><span class="price-tenure">/yr</span>
                                              </div>
                                              <div class="pricing-features">
                                                  <ul>
                                                      <li><?php echo $price_educators?> Educators</li>
                                                      <li>Unlimited users</li>
                                                      <li>GST included</li>
                                                  </ul>
                                              </div>
                                              <div class="pricing-action">
                                                  <a href="#" class="button button-pink button-lg"><?php echo $button_text?></a>
                                              </div>
                                          </div>
                                      </div>
                                   
                                   
                                   <?php 
                                    }
                                   }?>
                                   
                                  </div>
                             </div>
                             
                             <p class="center">*Only credit card payments are accepted for monthly billings</p>
                         </div>
                            <!-- end tab 2-->
                        </div>
                    </div>
            
            
            	<div class="section nopadding nobottommargin reasons noborder">
                <div class="container clearfix nobottommargin topmargin-lg">
                    <div class="row clearfix nomargin">
                        <div class="col_full">
                        	<div class="heading-block center">
                            	<h2><?php echo $section_title;?></h2>
                                
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix nomargin">
                        <div class="col_one_fourth">
                            <div class="feature-box fbox-center fbox-effect">
                                <div class="fbox-icon">
                                    <i class="icon-pricing1"></i>
                                </div>
                                <h3><?php echo $reason_title1;?></h3>
                                <p><?php echo $reason_text1;?></p>
                            </div>
                        </div>
                        <div class="col_one_fourth">
                            <div class="feature-box fbox-center fbox-effect">
                                <div class="fbox-icon">
                                    <i class="icon-pricing2"></i>
                                </div>
                                <h3><?php echo $reason_title2;?></h3>
                                <p><?php echo $reason_text2;?></p>
                            </div>
                        </div>
                        <div class="col_one_fourth">
                            <div class="feature-box fbox-center fbox-effect">
                                <div class="fbox-icon">
                                    <i class="icon-pricing3"></i>
                                </div>
                                <h3><?php echo $reason_title3;?></h3>
                                <p><?php echo $reason_text3;?></p>
                            </div>
                        </div>
                        <div class="col_one_fourth col_last">
                            <div class="feature-box fbox-center fbox-effect">
                                <div class="fbox-icon">
                                    <i class="icon-pricing4"></i>
                                </div>
                                <h3><?php echo $reason_title4;?></h3>
                                <p><?php echo $reason_text4;?></p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>

			</div>
            <?php include 'cta.php';?>
    		
		</section><!-- #content end -->

		<?php include 'footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>