<?php /**
 * Template Name: Features
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */	
$pageID = get_the_ID();
$meta_description = get_field("seo_meta_description", $pageID);
$meta_title = get_field("seo_meta_title", $pageID);
$page_title = get_field("page_title", $pageID );
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'header.php';?>

	<!-- Document Title
	============================================= -->
    <meta name="description" content="<?php echo $meta_description;?>"/>
	<title><?php bloginfo( 'name' ); ?> | <?php echo $meta_title;?></title>

</head>

<body class="stretched features" data-loader-color="#E57B8D">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'menu.php';?>
        
        <section id="slider" class="slider-parallax hero-features">
            
			<div class="container clearfix">
                <div class="heading-block nobottomborder col_half">
                    
					<h1>
						<?php echo $page_title;?>
					</h1>
                </div>
            </div>
		</section>
        

		<!-- Content
		============================================= -->
		<?php include 'qualities.php';?>
        <section id="content">
			<div class="nopadding content-wrap">
            
            
            
            <!-- intro -->
            
                <div class="section nopadding nomargin">
                    <div class="container clearfix nobottommargin topmargin-lg">
                        <div class="row clearfix nomargin">
                            <div class="col_full nomargin">
                                <div class="heading-block center nomargin">
                                <?php
                                    $page_subtitle = get_field("page_subtitle", $pageID );
                                    ?>
                                    <h2>
                                        <?php echo $page_subtitle;?>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section id="feature-rows">
                        <!--Feature -->
                        <?php 
                        $feat_query = new WP_Query();
                        $features = get_page_by_title('Features');
                        $args = array(
                            'post_type' => 'page',
                            'post_parent' => $features->ID,
                            'ignore_sticky_posts' => true,
                            'fields' => 'ids',
							'order' =>'ASC',
							'orderby' => 'date'
                        );
                        $feat_pages = $feat_query->query($args);	
                        $feature_children = get_page_children( $features -> ID, $feat_pages );
                        foreach ($feat_pages as $feat) {
    
                            $feature_title =  get_field("feature_title", $feat);
                            $feature_subtitle  = get_field("feature_subtitle", $feat);
                            $feature_text =  get_field("feature_text", $feat);
                            $feature_qa = get_field("quality_area", $feat);
                            $feature_image = get_field("feature_image", $feat);
                        
                       
                        ?>
                        <div class="section nopadding nomargin noborder">
                            <div class="container clearfix nobottommargin topmargin">
                                <div class="row clearfix">
                                   <div class="col_full">
                                        <div class="heading-block center">
                                            <h2 class="smallerh2"><?php
                                            echo $feature_title;
                                            ?></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col_one_fourth">
                                        <div class="heading-block">
                                            <h3><?php echo $feature_subtitle;?></h3>
                                        </div>
                                        <p><?php echo $feature_text;?></p>
                                    </div>
                                    <div class="col_one_fourth">
                                        <div class="heading-block">
                                            <h3>Key Quality Areas</h3>
                                        </div>
                                        
                                        <?php foreach ($feature_qa as $qa){?>
                                            <div class="fbox-icon qaicon qaicon<?php echo ($qa);?>"></div>
                                        <?php }?>
                                    </div>
                                    
                                    <div class="col_half col_last nobottommargin">
                                        <img src="<?php echo $feature_image[url];?>" alt="<?php echo $feature_image[alt];?>" />
                                     </div>
                                                                    
                                    
                                </div>
                            </div>
                        </div>
                        <?php
                        }
        
                        ?>
                    </section>
                            
                </div>
            </div>
            <?php include 'cta.php';?>
    
		</section><!-- #content end -->

		<?php include 'footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>