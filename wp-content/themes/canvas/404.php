<?php /**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'header.php';?>

	<!-- Document Title
	============================================= -->
	<title><?php bloginfo( 'name' ); ?> |  Oh no! 404!</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'menu.php';?>
        

		<!-- Content
		============================================= -->
		<section id="slider" class="slider-parallax full-screen dark error404-wrap" style="background: url(<?php echo esc_url( get_template_directory_uri() ); ?>/images/404.png) center;">
			<div class="container vertical-middle center clearfix">
				<div class="error404">404</div>
				<div class="heading-block nobottomborder">
					<h4>Oh no! The page you were looking for couldn't be found.</h4>
					<span>Return to <a href="javascript:history.back()">where you were</a> or <a href="/">return home</a>.</span>
				</div>
			</div>
		</section>
			
				

		<?php include 'footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>