<?php /**
 * Template Name: Terms
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */	
$pageID = get_the_ID();
$meta_description = get_field("seo_meta_description", $pageID);
$meta_title = get_field("seo_meta_title", $pageID);
$page_title = get_field("page_title", $pageID );
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'header.php';?>

	<!-- Document Title
	============================================= -->
	<meta name="description" content="<?php echo $meta_description;?>"/>
	<title><?php bloginfo( 'name' ); ?> | <?php echo $meta_title;?></title>
</head>

<body class="stretched privacy" data-loader-color="#E57B8D">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'menu.php';?>
        
        <section id="slider" class="slider-parallax hero-terms dark">
            
			<div class="container clearfix">
                <div class="heading-block nobottomborder col_half">
                    
					<h1>
						<?php echo $page_title;?>
					</h1>
                </div>
            </div>
		</section>
        

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="nopadding content-wrap">

            
            <!-- intro -->
            
            <div class="section nopadding bottommargin noborder">
                <div class="container clearfix nobottommargin topmargin-lg">
                    <div class="row clearfix nomargin">
                        <div class="col_full nomargin">
                            <div class="heading-block center nomargin">
                            <?php
                                $page_subtitle = get_field("page_subtitle", $pageID );
                                ?>
                                <h2>
                                    <?php echo $page_subtitle;?>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <section id="content">
                    <div class="nopadding content-wrap">
                            <div class="container clearfix nobottommargin topmargin-lg">
                                <div class="row clearfix nomargin">
                                    
                                    <div class="col_full heading-block nobottommargin">
                                        <?php
										$contentID = get_post($pageID);
										$content = apply_filters('the_content', $contentID->post_content);
										echo $content;
										?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                            
                </div>
            </div>
    
		</section><!-- #content end -->

		<?php include 'footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>