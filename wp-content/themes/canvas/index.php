<?php /**
 * Template Name: Home
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */	
$pageID = 5;
$page_title = get_field("page_title", $pageID );
$meta_description = get_field("seo_meta_description", $pageID);
$hero_button_text = get_field("cta_button_text", $pageID );
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'header.php';?>

	<!-- Document Title/Description
	============================================= -->
    
    <meta name="description" content="<?php echo $meta_description;?>"/>
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>

</head>

<body class="stretched home" data-loader-color="#E57B8D">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'menu.php';?>
        
        <section id="slider" class="slider-parallax hero-home">
            
			<div class="container clearfix">
                <div class="heading-block center nobottomborder col_half">
                    
					<h1>
						<?php echo $page_title;?>
					</h1>
                    <a href="http://www.complycare.com.au/sign-up" class="button button-xlarge button-large button-pink"><?php echo $hero_button_text;?> <i class="icon-circle-arrow-right"></i></a>
                </div>
            </div>
		</section>
        

		<!-- Content
		============================================= -->
		<?php include 'qualities.php';?>
        <section id="content">
			<div class="nopadding content-wrap">
            
            
            
            
            <!-- intro -->
            <div class="section nopadding nomargin">
                    <div class="container clearfix nobottommargin topmargin-lg">
                        <div class="row clearfix nomargin">
                        	<div class="col_full">
                            	<div class="heading-block center">
                            	<?php
									$page_subtitle = get_field("page_subtitle", $pageID );
									?>
									<h2>
										<?php echo $page_subtitle;?>
									</h2>
                            	</div>
                            </div>
                            <div class="col_half heading-block nobottommargin">
                               	<h3><?php
								$intro_heading = get_field("intro_subtitle", $pageID );
								echo $intro_heading;?>
                                </h3>
                                <p><?php
								$intro_text = get_field("intro_text", $pageID );
								echo $intro_text;?>
                                </p>
                            </div>
                            <div class="col_half nobottommargin center col_last">
                            	<?php
								$intro_image = get_field("intro_image", $pageID );
								$intro_video_link = get_field("intro_video_link", $pageID );
								?>
                                <a href="<?php echo $intro_video_link;?>" data-lightbox="iframe">
                                	<img src="<?php echo $intro_image[url];?>" alt="<?php echo $intro_image[alt];?>" data-animate="fadeInUp"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                
            
            
            	
               <!-- a better way section -->
               <div id="betterway" class="section nopadding nomargin noborder dark">
                    <div class="container clearfix nobottommargin">
                        <div class="row clearfix nomargin">
                            <div class="col_full nobottommargin">
                                <div class="heading-block center topmargin-lg">
                                    <?php 
									
									$betterwayID = 148;
									$betterway_title = get_field("section_title", $betterwayID );
									$betterway_text = get_field("section_text", $betterwayID );
									?>
									<h2>
										<?php echo $betterway_title;?>
									</h2>
                                    <p><?php echo $betterway_text;?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tabs clearfix" id="tabs">
                    	<?php 		$child_count = 0;
									$betterway_query = new WP_Query();
									$betterway = get_page_by_title('Better Way');
									$args = array(
										'post_type' => 'page',
										'post_parent' => $betterway->ID,
										'ignore_sticky_posts' => true,
										'fields' => 'ids',
									);
									$betterway_pages = $betterway_query->query($args);		
									$betterway_children = get_page_children( $betterway -> ID, $betterway_pages );
									?>
                    
                    	<div class="col_full nobottommargin center tab-nav">
                            <div class="container clearfix nobottommargin">
                                <div class="row clearfix nomargin">
                                    <ul class="clearfix">
                                    <?php foreach ($betterway_pages as $tab) {
										$tab_title = get_field("tab_title", $tab);
										$tab_id = get_field("tab_id", $tab);
										?>
                                        <li class="nomargin"><a href="#<?php echo $tab_id;?>" ><?php echo $tab_title;?></a></li>
                                    <?php
                                    }
									?>
                                     </ul>
                                 </div>
                             </div>
                       	</div>
                        <div class="tab-container">
                        	<?php foreach ($betterway_pages as $tab) {
							  $tab_id = get_field("tab_id", $tab);
							  $old_img = get_field("old_way_image", $tab);
							  $old_text = get_field("old_way_text", $tab);
							  $new_img = get_field("new_way_image", $tab);
							  $new_text = get_field("new_way_text", $tab);
							  ?>
												
                            <div class="tab-content clearfix" id="<?php echo $tab_id;?>">
                                <!--<div class="betterway-img nomargin"></div>-->
                                <div class="container clearfix bottommargin">
                                    <div class="row clearfix nomargin">
                                        <div class="col_half">
                                        	<div class="betterway-img nomargin">
                                            	<img src="<?php echo $old_img[url];?>" alt="<?php echo $old_img[alt];?>"/>
                                            </div>
                                            <div class="align-right"><?php echo $old_text;?></div>
                                        </div>
                                        <div class="col_half col_last">
                                        	<div class="betterway-img nomargin">
                                            	<img src="<?php echo $new_img[url];?>" alt="<?php echo $new_img[alt];?>"/>
                                            </div>
                                            <div class="align-left"><?php echo $new_text;?></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <?php
								}
							?>
                            <!--<div class="tab-content clearfix" id="tab-2">
                                <div class="container clearfix bottommargin">
                                    <div class="row clearfix nomargin">
                                        <div class="col_half">
                                        	<div class="betterway-img betterway-img1 nomargin"></div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                        </div>
                                        <div class="col_half col_last">
                                        	<div class="betterway-img betterway-img2 nomargin"></div>
                                            <p>HDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                           
                           
                           
                        </div>
                    </div>
                    
                </div>
                
                
                <div id="testimonials" class="section parallax dark nomargin noborder" data-stellar-background-ratio="0.4">
                	<div class="container clearfix nobottommargin">
                        <div class="row clearfix nomargin">
                            <div class="col_full nobottommargin ">
                    			<div class="heading-block center topmargin-sm nobottommargin">
                                   <?php
								   	$testiID = 9;
									$testi_title = get_field("section_title", $testiID );
								   ?> 
									<h2><?php echo $testi_title;?> </h2>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fslider testimonial" data-animation="fade" data-arrows="false">
                        <div class="flexslider">
                            <div class="slider-wrap">
                            
                            	<?php 
									$testi_query = new WP_Query();
									$testimonial = get_page_by_title('Testimonials');
									$args = array(
										'post_type' => 'page',
										'post_parent' => $testimonial->ID,
										'ignore_sticky_posts' => true,
										'fields' => 'ids',
									);
									$testi_pages = $testi_query->query($args);		
									$testimonial_children = get_page_children( $testimonial -> ID, $testi_pages );
									
									foreach ($testi_pages as $testi) {
										$testi_img = get_field("testimonial_avatar", $testi);
										$testi_content = get_field("testimonial_content", $testi);
										$testi_name = get_field("testimonial_name", $testi);
										$testi_title = get_field("testimonial_title", $testi);
										
										//$page = get_page_by_title($testimonial_titles);
										//$content = apply_filters('the_content', $page->post_content);
										//print_r ($testimonial_titles);
										?>
                                    
                                    <div class="slide">
                                        <div class="col_one_third bottommargin-sm">
                                        	<?php //print_r ($testi_img);?>
                                            <img src="<?php echo $testi_img[url];?>" alt="<?php echo $testi_img[alt];?>"/>
                                        
                                        </div>
										<div class="col_two_third col_last topmargin-sm testi-content">
                                        	<p><?php
											echo ($testi_content);
											?><br/>
											<strong>
											<?php

											echo ($testi_name);
											?>
                                            </strong>
                                            
                                            <em>
											<?php
											echo ($testi_title);
										
                                       		?>
                                        	</em>
                                            </p>
                                        </div>
                                    </div>
                                    <?php
                                    }

									?>
                             </div>
                         </div>
                     </div>
                 </div>
                 
                 <?php include 'cta.php';?>
		</section><!-- #content end -->

		<?php include 'footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>