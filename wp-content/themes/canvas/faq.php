<?php /**
 * Template Name: FAQ
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$pageID = get_the_ID();
$meta_description = get_field("seo_meta_description", $pageID);
$meta_title = get_field("seo_meta_title", $pageID);
$page_title = get_field("page_title", $pageID );
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'header.php';?>

	<!-- Document Title
	============================================= -->
	<meta name="description" content="<?php echo $meta_description;?>"/>
	<title><?php bloginfo( 'name' ); ?> | <?php echo $meta_title;?></title>

</head>

<body class="stretched faq" data-loader-color="#E57B8D">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'menu.php';?>
        
        <section id="slider" class="slider-parallax dark hero-faq">
            
			<div class="container clearfix">
                <div class="heading-block nobottomborder col_half">
                    
					<h1>
						<?php echo $page_title;?>
					</h1>

                </div>
            </div>
		</section>
        

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="nopadding content-wrap">
            
            <!-- intro -->
            <div class="section nopadding nomargin">
                    <div class="container clearfix nobottommargin topmargin-lg">
                        <div class="row clearfix nomargin">
                        	
                            	<?php
								$catID=100;
								$cat = get_post($catID);
								$catcontent = apply_filters('the_content', $cat->post_content);
								echo $catcontent;
								?>
                            
                        </div>
                    </div>
                </div>
                
            

                 <?php include 'cta.php';?>
    
		</section><!-- #content end -->

		<?php include 'footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>