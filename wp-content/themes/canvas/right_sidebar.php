<!-- Sidebar
  ============================================= -->
  <div class="sidebar nobottommargin col_last clearfix col_one_fourth">
      <div class="sidebar-widgets-wrap">
          <div class="widget clearfix cta dark center">
          	<div class="heading-block center bottommargin-sm">
              <h2>Get started</h2>
              <p>Try Complycare FREE for 30 days, no obligations!</p>
              </div>
              <a href="http://www.complycare.com.au/sign-up" class="button button-large button-royalblue nomargin">Sign up now</a>
          </div>
          
          <?php get_sidebar(); ?>
          
          <div class="widget clearfix">
              <h4>Twitter Feed</h4>
              <div class="widget clearfix">
                  <a class="twitter-timeline" data-chrome="noheader" data-width="240" data-tweet-limit="2" data-dnt="true" data-link-color="#0356A2" href="https://twitter.com/Complycare">Tweets by Complycare</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
              </div>
          </div>
          
      </div>
  </div><!-- .sidebar end -->