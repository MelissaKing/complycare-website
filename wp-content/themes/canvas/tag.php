<?php /**
 * Template Name: Tag
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'header.php';?>

	<!-- Document Title
	============================================= -->
	<title>Draftee | Blog</title>

</head>

<body class="stretched blog_home">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'menu.php';?>

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
                	<!-- Posts
						============================================= -->
						<div id="posts">


                        <!-- Post Content
                        ============================================= -->
                        <div class="postcontent nobottommargin clearfix">
                        	<div class="col_full">
                            	<div class="heading-block center bottommargin-lg">
                                	<h1><?php printf( __( 'Posts tagged "%s', 'twentyfourteen' ), single_tag_title( '', false ) ); ?>"</h1>
                                </div>
                            </div>
                        	<?php
								// Start the Loop.
								while ( have_posts() ) : the_post();
			
									/*
									 * Include the post format-specific template for the content. If you want to
									 * use this in a child theme, then include a file called called content-___.php
									 * (where ___ is the post format) and that will be used instead.
									 */
									get_template_part( 'content_preview', get_post_format() );
			
								endwhile;
								// Previous/next page navigation.
								
			
							?>
                           
                            
                           <!-- Pagination
						============================================= -->
						<div class="clear"></div>
                            <div class="pager margintop-lg">
                            <?php // Previous/next page navigation.
                            the_posts_pagination( array(
                                'prev_text'          => __( '&laquo;', 'Draftee' ),
                                'next_text'          => __( ' &raquo;', 'Draftee' ),
                                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyfifteen' ) . ' </span>',
                            ) );?>
                            </div><!-- .pager end -->
    
                        </div><!-- .postcontent end -->
                	</div>
					<?php include 'right_sidebar.php';?>

				</div>
<div class="section footer-stick dark orange noborder">
					<div class="container clearfix nobottomborder">
                    	<div class="column col_half">
                            <div id="section-buy" class="heading-block nobottomborder bottommargin-sm page-section">
                                <h3>Creating an account is free and only takes a few seconds...</h3>
                             </div>
                             <p>Save time and money caused by delays from inexperienced drafting services. Start your first project with Draftee and experience the difference it can make to your business.</p>
                        </div>
						<div class="center col_half col_last">
							<a href="http://app.draftee.com.au/register" class="topmargin-lg button button-round button-border button-orange button-xlarge nobottommargin">Get started</a>
						</div>
					</div>
				</div>
			</div>

		
		</section><!-- #content end -->

		<?php include 'footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>