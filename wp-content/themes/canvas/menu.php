<!-- Header
		============================================= -->
		<header id="header" class="page-section dark">
			<div id="header-wrap">
				<div class="container clearfix">
					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
                    	<?php
							canvas_the_custom_logo();
							
							$current = $_SERVER['REQUEST_URI'];
							$currentPage = basename($current);
							//echo $currentPage;

						?>
                        
                        <script type="text/javascript">
						$(function() {
						   $("#primary-menu li").click(function() {
							  // remove classes from all
							  $("#primary-menu li").removeClass("current");
							  // add class to the one we clicked
							  $(this).addClass("current");
						   });
						});
                        </script>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">

						<ul>
                       	 	<li <?php if ($currentPage === 'features'){
								echo 'class="current"';
								}?>
                                ><a href="/complycare/features/"><i class="icon-cloudapp icon-lg"></i><div>Features</div></a></li>
							<li <?php if ($currentPage === 'pricing'){
								echo 'class="current"';
								}?>
                                ><a href="/complycare/pricing/"><i class="icon-ticket icon-lg"></i><div>Pricing</div></a></li>
							<li <?php if ($currentPage === 'blog'){
								echo 'class="current"';
								}?>
                                ><a href="/complycare/blog/"><i class="icon-book icon-lg"></i><div>Blog</div></a></li>
                            <li <?php if ($currentPage === 'faq'){
								echo 'class="current"';
								}?>
                                ><a href="/complycare/faq/"><i class="icon-question icon-lg"></i><div>FAQ</div></a></li>
                            <li <?php if ($currentPage === 'about'){
								echo 'class="current"';
								}?>
                                ><a href="/complycare/about/"><i class="icon-users icon-lg"></i><div>About</div></a></li>
                            <li <?php if ($currentPage === 'contact'){
								echo 'class="current"';
								}?>
                                ><a href="/complycare/contact/"><i class="icon-phone icon-lg"></i><div>Contact</div></a></li>
                            <li><a class="button button-large button-pink" href="http://complycare.com.au/sign-up"><i class="icon-signin icon-lg"></i>Sign up FREE<br/><span class="phone">1300 429 500</span></a></li>
	                        <li><a class="button button-large button-blue" href="http://complycare.com.au/login"><i class="icon-user icon-lg"></i></a></li>
						</ul>
					</nav><!-- #primary-menu end -->
				</div>
			</div>
		</header><!-- #header end -->