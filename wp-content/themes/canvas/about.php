<?php /**
 * Template Name: About
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */	
$pageID = get_the_ID();
$meta_description = get_field("seo_meta_description", $pageID);
$meta_title = get_field("seo_meta_title", $pageID);
$page_title = get_field("page_title", $pageID );
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'header.php';?>

	<!-- Document Title
	============================================= -->
	<meta name="description" content="<?php echo $meta_description;?>"/>
	<title><?php bloginfo( 'name' ); ?> | <?php echo $meta_title;?></title>
</head>

<body class="stretched about" data-loader-color="#E57B8D">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'menu.php';?>
        
        <section id="slider" class="slider-parallax dark hero-about">
            
			<div class="container clearfix">
                <div class="heading-block nobottomborder col_half">
					
					<h1>
						<?php echo $page_title;?>
					</h1>
                </div>
            </div>
		</section>
        

		<!-- Content
		============================================= -->
		<?php include 'qualities.php';?>
        <section id="content">
			<div class="nopadding content-wrap">
            
            
            
            <!-- intro -->
            <div class="section noborder nomargin">
                    <div class="container clearfix topmargin">
                        <div class="row clearfix">
                            <div class="col_half">
                                <div class="heading-block">
                                    <?php 
									$about_title1 = get_field("about_title_1", $pageID ); 
									$about_text1 = get_field("about_text_1", $pageID );
									$about_img1 = get_field("about_image_1", $pageID );
									$about_title2 = get_field("about_title_2", $pageID );
									$about_text2 = get_field("about_text_2", $pageID );
									$about_img2 = get_field("about_image_2", $pageID );
									?>
                                    <h2><?php echo $about_title1;?></h2>
                            	</div>
                               	<p><?php echo $about_text1;?></p>
                                
                            </div>
                            <div class="col_half center col_last">
                            	<img src="<?php echo $about_img1[url];?>" alt="<?php echo $about_img1[alt];?>" data-animate="fadeInRight"/>
                            </div>
                        </div>
                        <div class="row clearfix topmargin">

                            <div class="col_half center">
                            	<img src="<?php echo $about_img2[url];?>" alt="<?php echo $about_img2[alt];?>" data-animate="fadeInLeft"/>
                            </div>
                            
                            <div class="col_half col_last">
                                <div class="heading-block">
                                    <h2><?php echo $about_title2;?></h2>
                            	</div>
                               	<p><?php echo $about_text2;?></p>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <?php include 'cta.php';?>
    
		</section><!-- #content end -->

		<?php include 'footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>