<?php /**
 * Template Name: Blog list
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'header.php';?>

	<!-- Document Title
	============================================= -->
	<title>Draftee | Blog</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'menu.php';?>
        

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
                	<!-- Posts
						============================================= -->
						<div id="posts" class="col_three_fourth">


                        <!-- Post Content
                        ============================================= -->
                        <div class="postcontent nobottommargin clearfix">
                        
                            <?php if ( have_posts() ) : ?>
    
                            <?php if ( is_home() && ! is_front_page() ) : ?>
                                <div class="entry-title">
                                    <h2><?php single_post_title(); ?></h2>
                                </div>
                            <?php endif; ?>
                
                            <?php
                            // Start the loop.
                            while ( have_posts() ) : the_post();
                
                                /*
                                 * Include the Post-Format-specific template for the content.
                                 * If you want to override this in a child theme, then include a file
                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                 */
                                get_template_part( 'content', get_post_format() );
                
                            // End the loop.
                            endwhile;
                
                            
							// If no content, include the "No posts found" template.
							else :
								get_template_part( 'content', 'none' );
					
							endif;
							?>
                            
                            <!-- Pagination
						============================================= -->
						<div class="clear"></div>
                            <div class="pager margintop-lg">
                            <?php // Previous/next page navigation.
                            the_posts_pagination( array(
                                'prev_text'          => __( '&laquo; Newer', 'Draftee' ),
                                'next_text'          => __( 'Older &raquo;', 'Draftee' ),
                                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
                            ) );?>
                            </div><!-- .pager end -->
    
                        </div><!-- .postcontent end -->
                	</div>
					<?php include 'right_sidebar.php';?>

				</div>

			</div>

		<?php include 'cta.php';?>
    
		</section><!-- #content end -->

		<?php include 'footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>