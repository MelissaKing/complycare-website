<div id="qualities" class="section noborder nomargin nopadding dark">
				<div class="container clear-bottommargin clearfix ">
                    <div class="heading-block title-center topmargin bottommargin-sm page-section">
                        <h2>Supporting you to achieve the highest level of compliance by following the National Quality Standard.</h2>
                    </div>
					<div class="row topmargin clearfix bottommargin-lg">
                        <ul>
                            <li class="col_one_seventh feature-box fbox-center fbox-effect nobottomborder" >
                                <div class="qa qa1 col_full fbox-icon" data-animate="fadeIn" data-delay="0"></div>
                                <div class="qabox qabox1">
                                	<i class="qaicon qaicon1"></i>
                                	<span>Helping educators implement programs and record assessments of a child's learning and development.</span>
                                	<a href="/complycare/features/"  class="button button-light button-small">More</a>
                                </div>
                            </li>
                            <li class="col_one_seventh feature-box fbox-center fbox-effect nobottomborder">
                                <div class="qa qa2 col_full fbox-icon" data-animate="fadeIn" data-delay="100"></div>
                                <div class="qabox qabox2">
                                	<i class="qaicon qaicon2"></i>
                                	<span>Implementing policies and procedures to ensure the health and safety of children.</span>
                                	<a href="/complycare/features/"  class="button button-light button-small">More</a>
                                </div>
                            </li>
                            <li class="col_one_seventh feature-box fbox-center fbox-effect nobottomborder">
                                <div class="qa qa3 col_full fbox-icon" data-animate="fadeIn" data-delay="200"></div>
                                <div class="qabox qabox3">
                                	<i class="qaicon qaicon3"></i>
                                	<span>Creating a safe, suitable indoor and outdoor environment for play and learning.</span>
                                	<a href="/complycare/features/"  class="button button-light button-small">More</a>
                                </div>
                            </li>
                            <li class="col_one_seventh feature-box fbox-center fbox-effect nobottomborder">
                                <div class="qa qa4 col_full fbox-icon" data-animate="fadeIn" data-delay="300"></div>
                                <div class="qabox qabox4">
                                	<i class="qaicon qaicon4"></i>
                                	<span>Monitoring the provisions and qualifications of your educators made simple and easy.  </span>
                                	<a href="/complycare/features/"  class="button button-light button-small">More</a>
                                </div>
                                
                            </li>
                            <li class="col_one_seventh feature-box fbox-center fbox-effect nobottomborder">
                                <div class="qa qa5 col_full fbox-icon" data-animate="fadeIn" data-delay="400"></div>
                                <div class="qabox qabox5">
                                	<i class="qaicon qaicon5"></i>
                                	<span>Fostering positive relationships with educators to help them have safe and supportive relationships with the children. </span>
                                	<a href="/complycare/features/"  class="button button-light button-small">More</a>
                                </div>
                            </li>
                            <li class="col_one_seventh feature-box fbox-center fbox-effect nobottomborder">
                                <div class="qa qa6 col_full fbox-icon" data-animate="fadeIn" data-delay="500"></div>
                                <div class="qabox qabox6">
                                	<i class="qaicon qaicon6"></i>
                                	<span>Developing partnerships with families and communities to achieve quality outcomes for children. </span>
                                	<a href="/complycare/features/"  class="button button-light button-small">More</a>
                                </div>
                            </li>
                            <li class="col_one_seventh feature-box fbox-center fbox-effect nobottomborder col_last">
                                <div class="qa qa7 col_full fbox-icon" data-animate="fadeIn" data-delay="600"></div>
                                <div class="qabox qabox7">
                                	<i class="qaicon qaicon7"></i>
                                	<span>Supporting you to become a leader through making documentation easy and shared values a part of your operation.</span>
                                	<a href="/complycare/features/"  class="button button-light button-small">More</a>
                                </div>
                            </li>
                        </ul>
					</div>    
				</div>
			</div>