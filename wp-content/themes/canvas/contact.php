<?php /**
 * Template Name: Contact
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$pageID = get_the_ID();
$meta_description = get_field("seo_meta_description", $pageID);
$meta_title = get_field("seo_meta_title", $pageID);
$page_title = get_field("page_title", $pageID );
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<?php include 'header.php';?>

	<!-- Document Title
	============================================= -->
	<meta name="description" content="<?php echo $meta_description;?>"/>
	<title><?php bloginfo( 'name' ); ?> | <?php echo $meta_title;?></title>
</head>

<body class="stretched contact" data-loader-color="#E57B8D">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php include 'menu.php';?>
        
        <section id="slider" class="slider-parallax dark hero-contact">
            
			<div class="container clearfix">
                <div class="heading-block nobottomborder col_half">
                    
					<h1>
						<?php echo $page_title;?>
					</h1>
                </div>
            </div>
		</section>
        

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="nopadding content-wrap">
           
            
            <!-- intro -->
            <div  id="contact"  class="section nopadding nomargin">
                    <div class="container clearfix nobottommargin topmargin-lg">
                        	
                    <!-- Contact Form
                    ============================================= -->

                        <div class="row clearfix nomargin">
                            
                            <div class="heading-block bottommargin-sm center">
                                <?php
                                $page_subtitle = get_field("page_subtitle", $pageID );
                                ?>
                                <h2>
                                    <?php echo $page_subtitle;?>
                                </h2>
                            </div>
                            
                            <div class="col_half bottommargin-lg contact-details">
                                <div class="col_full">
                                    <div class="heading-block bottommargin-sm">
                                        <h3>Find us</h3>
                                    </div>
                                    <?php
                                    $email = get_field("email", $pageID );
                                    $phone = get_field("phone", $pageID );
                                    $address = get_field("address", $pageID );
                                    ?>
                                    <i class="icon-email3"></i><a href="mailto:<?php echo $email;?>"><?php echo $email;?></a><br/>
                                    <i class="icon-call"></i><span><?php echo $phone;?></span><br/>
                                    <i class="icon-location"></i><span><?php echo $address; ?></span>
                                </div>
                                
                                <!-- Contact Info End -->
                                
                            
                                <div id="contact-form-result" data-notify-type="success" data-notify-msg=""></div>
                                <div id="contact-response"></div>
                                <div class="heading-block bottommargin-sm">
                                        <h3>Contact us</h3>
                                    </div>
                                <form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">
            
                                    <div class="form-process"></div>
            
                                    <div class="col_full">
                                        <label for="contactform_name">Name <small>*</small></label>
                                        <input type="text" id="contactform_name" name="contactform_name" value="" class="sm-form-control required" />
                                    </div>
                                    
                                    
                                    <div class="clear"></div>
            
                                    <div class="col_half">
                                        <label for="contactform_email">Email <small>*</small></label>
                                        <input type="email" id="contactform_email" name="contactform_email" value="" class="required email sm-form-control" />
                                    </div>
            
                                    <div class="col_half col_last">
                                        <label for="contactform_phone">Phone</label>
                                        <input type="text" id="contactform_phone" name="contactform_phone" value="" class="sm-form-control" />
                                    </div>
                                    <div class="clear"></div>
                                    <div class="col_full">
                                        <label for="contactform_message">Message <small>*</small></label>
                                        <textarea class="required sm-form-control" id="contactform_message" name="contactform_message" rows="6" cols="30"></textarea>
                                    </div>
            
                                    <div class="col_full hidden">
                                        <input type="text" id="contactform_botcheck" name="contactform_botcheck" value="" class="sm-form-control" />
                                    </div>
            
                                    <div class="col_full">
                                        <button name="submit" type="submit" id="submit-button" tabindex="5" value="Submit" class="button button-large nomargin button-blue pull-right">Send Email</button>
                                    </div>
                                </form>
                                <script type="text/javascript">
            
                                $("#template-contactform").validate({
                                        submitHandler: function(form) {
                                            $('#contact-response').html();
                                            $.ajax({
                                                type: 'post',
                                                url: '/contact-submit',
                                                data: $(form).serializeArray(),
                                                success: function() {
                                                    $('#contact-form-result').attr('data-notify-msg', '<div class="center topmargin-sm" data-animate="fadeInUp" data-delay="100"><h4 class="nobottommargin">Thanks for your email <i class="icon-ok-sign icon-large"></i></h4><p>We will be in touch shortly</p></div>').html('');
                                                    $.magnificPopup.close();
                                                    SEMICOLON.widget.notifications($('#contact-form-result'));
                                                }, error: function(result) {
                                                    $('#contact-response').html('<div class="style-msg errormsg"><div class="sb-msg"><i class="icon-remove"></i><strong>Something went wrong!</strong> Please try again, or call us directly on (03) 9563 0543.</div></div>');
                                                }
                                            });
                                        }
                                    });
                                </script>
            
                                <!--<script type="text/javascript">
            
                                    $("#template-contactform").validate({
                                        submitHandler: function(form) {
                                            $('.form-process').fadeIn();
                                            $(form).ajaxSubmit({
                                                target: '#contact-form-result',
                                                success: function() {
                                                    $('.form-process').fadeOut();
                                                    $('#template-contactform').find('.sm-form-control').val('');
                                                    $('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
                                                    SEMICOLON.widget.notifications($('#contact-form-result'));
                                                }
                                            });
                                        }
                                    });
            
                                </script>-->
            
                            </div><!-- Contact Form End -->

                        <!-- Google Map
                        ============================================= -->
                        <div class="col_half col_last topmargin-sm bottommargin-lg">
                            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxFnZ6KYVwIVrFmcUdMU2qgSRQiCkW-NQ&sensor=false&extension=.js"></script>
                            <script> 
                                google.maps.event.addDomListener(window, 'load', init);
                                var map;
                                function init() {
                                    // If document (your website) is wider than 480px, isDraggable = true, else isDraggable = false
                                    var isDraggable = $(document).width() > 480 ? true : false;
                                    var mapOptions = {
                                        center: new google.maps.LatLng(-37.8986431,145.0880675),
                                        zoom: 16,
                                        zoomControl: true,
                                        zoomControlOptions: {
                                            style: google.maps.ZoomControlStyle.DEFAULT,
                                        },
                                        disableDoubleClickZoom: true,
                                        mapTypeControl: true,
                                        mapTypeControlOptions: {
                                            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                                        },
                                        scaleControl: true,
                                        scrollwheel: isDraggable,
                                        panControl: true,
                                        streetViewControl: true,
                                        draggable: isDraggable,
                                        overviewMapControl: true,
                                        overviewMapControlOptions: {
                                            opened: false,
                                        },
                                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                                        styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#ff6a6a"},{"lightness":"0"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ef4581"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#0a213c"},{"weight":"2.53"}]},{"featureType":"road.highway","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"weight":"5.19"}]},{"featureType":"road.highway","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#e57c8e"},{"lightness":"62"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"lightness":"75"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#0a213c"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"transit.station.bus","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"transit.station.rail","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"transit.station.rail","elementType":"labels.icon","stylers":[{"weight":"0.01"},{"hue":"#0084ff"},{"lightness":"0"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#0356a3"},{"lightness":"25"},{"saturation":"-23"}]}],
                                    }
                                    var mapElement = document.getElementById('google_map');
                                    var map = new google.maps.Map(mapElement, mapOptions);
                                    var locations = [
                            ['Complycare', 'a Quality Management System for Family Day Care', '(03) 9563 0543', 'info@complycare.com.au', 'www.complycare.com.au', -37.8986431,145.0880675, '<?php echo esc_url( get_template_directory_uri() ); ?>/images/map-marker.png']
                                    ];
                                    for (i = 0; i < locations.length; i++) {
                                        if (locations[i][1] =='undefined'){ description ='';} else { description = locations[i][1];}
                                        if (locations[i][2] =='undefined'){ telephone ='';} else { telephone = locations[i][2];}
                                        if (locations[i][3] =='undefined'){ email ='';} else { email = locations[i][3];}
                                       if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
                                       if (locations[i][7] =='undefined'){ markericon ='';} else { markericon = locations[i][7];}
                                        marker = new google.maps.Marker({
                                            icon: markericon,
                                            position: new google.maps.LatLng(locations[i][5], locations[i][6]),
                                            map: map,
                                            title: locations[i][0],
                                            desc: description,
                                            tel: telephone,
                                            email: email,
                                            web: web
                                        });
                                        
                                    link = '';     }
                                    
                                    }
                                    
                            </script>
                            <style>
                                #google_map {
                                    height:625px;
                                    width:100%;
                                }
                                @media (max-width: 767px) {
                                    #google_map {
                                        height:300px;
                                        top:20px;
                                    }
                                }
                                .gm-style-iw * {
                                    display: block;
                                    width: 100%;
                                }
                                .gm-style-iw h4, .gm-style-iw p {
                                    margin: 0;
                                    padding: 0;
                                }
                                .gm-style-iw a {
                                    color: #4272db;
                                }
                            </style>
                            <div id="google_map"></div><!-- Google Map End -->
                            <div class="clear"></div>

                        </div>
                    </div>

                </div>
             </div>
             <?php include 'cta.php';?>
    
		</section><!-- #content end -->

		<?php include 'footer.php';?>

	</div><!-- #wrapper end -->
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/functions.js"></script>

</body>
</html>